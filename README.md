# Backend Zaida

## Technologies:
- Node
- Mongo
- Mongoose
- Express
- Swagger
- Docker

## Requirements:
- *docker* & *docker-compose* installed
  
## How to run:
- `docker-compose up -d`

## Swagger API's definition:

- `/api/swagger`