// services.js
const jwt = require('jwt-simple');
const moment = require('moment');
const config = require('./config');
const Turn = require('./models/Turn')

exports.createToken = function (user) {
  if (!user) {
    return null
  }
  var payload = {
    sub: user._id,
    iat: moment().unix(),
    exp: moment().add(14, "days").unix(),
  };
  return jwt.encode(payload, config.TOKEN_SECRET);
};

exports.getNextTurnNumber = async function (turn) {
  /* const res = await Turn.findOne({ process: turn.process }, {}, { sort: { 'createdAt': -1 } }) */
  const res = await Turn.findOne({}, {}, { sort: { 'createdAt': -1 } })
  if (res) {
    prevNumber = 0
    if (res.number) prevNumber = res.number
    return prevNumber + 1;
  } else {
    return 0
  }
}

function send2Graylog(message, token, action) {
  var payload = Object.assign({}, message);
  if (token){
    payload.user = token
  }
  payload.action = action
  payload.backend = config.BACKEND_TAG
  console.log(JSON.stringify(payload))
}

exports.send2Graylog = send2Graylog

exports.closeOpenedTurns = async function () {
  Turn.find({ state: 'opened' }).then(docs => {
    docs.forEach(doc => {
      doc.pendingActions = 'Closed by cron'
      doc.state = 'closed'
      doc.save()
      _doc = doc._doc
      send2Graylog(_doc, 'system', 'update_turn');
    })
  })
  Turn.find({ state: null }).then(docs => {
    docs.forEach(doc => {
      doc.pendingActions = 'Closed by cron'
      doc.state = 'closed'
      doc.save()
      _doc = doc._doc
      send2Graylog(_doc, 'system', 'update_turn');
    })
  })
  // Turn.updateMany({ state: 'opened' },
  //   {
  //     "$set": {
  //       state: 'closed',
  //       pendingActions: 'Closed by cron'
  //     }
  //   },
  //   { "multi": true }, (err, writeResult) => { 
  //     console.log(writeResult)
  //   });
}