const mongoose = require('mongoose')
const { Schema } = mongoose

const Turn = new Schema({
    process: Object,
    products: [Object],
    notes: [String],
    state: String,
    pendingActions: String,
    number: Number,
    code: String,
    createdAt: Date,
    group: String,
    priority: Number,
    client: Object,
    time: String,
    employee: Object,
    origin: String
})

module.exports = mongoose.model('Turn', Turn)