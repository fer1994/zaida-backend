const mongoose = require('mongoose')
const { Schema } = mongoose

const Process = new Schema({
    name: String,
    description: String,
    suggestions: [String]
})

module.exports = mongoose.model('Process', Process)