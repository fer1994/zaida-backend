const mongoose = require('mongoose')
const { Schema } = mongoose

const Client = new Schema({
    name: String,
    lastName: String,
    email: String,
    phone: String

})

module.exports = mongoose.model('Client', Client)