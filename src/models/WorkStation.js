const mongoose = require('mongoose')
const { Schema } = mongoose

const WorkStation = new Schema({
    boxNumber: String,
    adress: String,
    switchEnable: Boolean,
})

module.exports = mongoose.model('WorkStation', WorkStation)