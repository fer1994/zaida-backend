const mongoose = require('mongoose')
const { Schema } = mongoose

const WorkStation = require('./WorkStation')

const Employee = new Schema({
    name: String,
    lastName: String,
    email: String,
    admin: Boolean,
    workStation: Object,
    password: String,
})

module.exports = mongoose.model('Employee', Employee)