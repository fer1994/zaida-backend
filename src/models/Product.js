const mongoose = require('mongoose')
const { Schema } = mongoose

const Product = new Schema({
    name: String,
    description: String,
    categories: [Object],
    amount: String,
    order: Number,
    tabColor: String,
    active: Boolean
})

module.exports = mongoose.model('Product', Product)