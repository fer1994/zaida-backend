// config.js
module.exports = {
  TOKEN_SECRET: process.env.TOKEN_SECRET || "tokenultrasecreto",
  BACKEND_TAG: process.env.BACKEND_TAG || 'localhost',
  MONGO_DNS: process.env.MONGO_DNS || 'localhost',
  PORT: process.env.PORT || 3000,
  HOST:'0.0.0.0'
};