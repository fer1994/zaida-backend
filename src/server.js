const express = require('express')
const http = require('http');
const morgan = require('morgan')
const mongoose = require('mongoose')
const swaggerUI = require('swagger-ui-express')
const swaggerDocument = require('./swagger')
const config = require('./config');
const _closeOpenedTurns = require('./scheduled_tasks')
const socketIO = require('socket.io');
// Definitions
const app = express()
/* dbURI Local uncomment this to run local mongo instance: */
//const dbURI = 'mongodb://localhost:27017/admin';
const dbURI = `mongodb://zuser:zpass@${config.MONGO_DNS}:27017/admin`
const mongooseOptions = { useNewUrlParser: true, auto_reconnect: true }
const db = mongoose.connection;
require('events').EventEmitter.prototype._maxListeners = 0;


// Database events
db.on('error', function(error) {
    mongoose.disconnect();
});
db.on('connected', function() {
    console.log('MongoDB connected!');
});
db.once('open', function() {
    console.log('MongoDB connection opened!');
});
db.on('reconnected', function() {
    console.log('MongoDB reconnected!');
});
db.on('disconnected', function() {
    console.log('MongoDB disconnected!');
    mongoose.connect(dbURI, mongooseOptions);
});

mongoose.connect(dbURI, mongooseOptions);


// // Allow CORS
// app.use(function(req, res, next) {
//     res.header("Access-Control-Allow-Origin", "*");
//     res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
//     res.header('Access-Control-Allow-Methods', 'PUT, POST, GET, DELETE, OPTIONS');
//     next();
// });

// Server setup
app.use(morgan('dev'))
app.use(express.json())
app.use(function(req, res, next) {
    res.setHeader('Access-Control-Allow-Origin', '*');
    res.setHeader("Access-Control-Allow-Headers", "Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");
    res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');
    res.setHeader('Access-Control-Allow-Credentials', true);
    next();
});

// Server routing
app.use('/api/settings', require('./routes/settings'))
app.use('/api/clients', require('./routes/clients'))
app.use('/api/employees', require('./routes/employees'))
app.use('/api/processes', require('./routes/processes'))
app.use('/api/products', require('./routes/products'))
app.use('/api/turns', require('./routes/turns'))
app.use('/api/workStations', require('./routes/workStations'))
app.use(express.static(__dirname + '/public'))
app.use('/api/swagger', swaggerUI.serve, swaggerUI.setup(swaggerDocument))


// Server start
const server = app.listen(config.PORT, config.HOST);
const io = socketIO.listen(server);

function updateListeners(io){   
    let users = Object.keys(io.sockets.connected).map(
        key => {
            return {name: io.sockets.connected[key].nickname,
                    status: io.sockets.connected[key].status,
                    station: io.sockets.connected[key].station,
                    email:io.sockets.connected[key].email}
        });
    io.emit('update-clients', users);
}


// Socket events
io.on('connection', (socket) => {
    socket.nickname = 'Anonymous';
    socket.status = '';

    socket.on('set-nickname', function(nickname) {
        socket.nickname = nickname;
        updateListeners(io);
    });

    socket.on('set-email', function(email) {
        socket.email = email;
        updateListeners(io);
    });

    socket.on('set-status', function(status) {
        socket.status = status;
        updateListeners(io);
    });

    socket.on('set-station', function(station) {
        socket.station = station;
        updateListeners(io);
    });

    socket.on('disconnect', (socket) => {
        updateListeners(io);
    });

    socket.on('broadcast-data', (socket) => {
        updateListeners(io);
    });
});
