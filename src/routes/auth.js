const express = require('express')
const authCtrl = require('../auth');

const router = express.Router()

router.post('/signup', authCtrl.emailSignup);
router.post('/login', authCtrl.emailLogin);

module.exports = router