const express = require('express')
var auth = require('../middleware')
const router = express.Router()

const Client = require('../models/Client')

// Get all Clients
router.get('/', auth.ensureAuthenticated, async (req, res) => {
    const clients = await Client.find()
    res.json({
        status: 200,
        clients
    })
})
// Get an especific Client
router.get('/:_id', auth.ensureAuthenticated, async (req, res) => {
    const client = await Client.findById(req.params._id)
    res.json({
        status: 200,
        client
    })
})
// Save a new Client
router.post('/', auth.ensureAuthenticated, async (req, res) => {
    const client = new Client(req.body)
    await client.save()
    res.json({
        status: 200
    })
})
// Edit an existing Client
router.put('/:_id', auth.ensureAuthenticated, async (req, res) => {
    await Client.findByIdAndUpdate(req.params._id, req.body)
    res.json({
        status: 200
    })
})
// Delete a Client
router.delete('/:_id', auth.ensureAuthenticated, async (req, res) => {
    await Client.findByIdAndDelete(req.params._id)
    res.json({
        status: 200
    })
})

module.exports = router