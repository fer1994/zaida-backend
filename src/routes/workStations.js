const express = require('express')
var auth = require('../middleware')
const router = express.Router()

const WorkStation = require('../models/WorkStation')
// Get all WorkStations
router.get('/', auth.ensureAuthenticated, async (req, res) => {
    const workStations = await WorkStation.find()
    res.json({
        status: 200,
        workStations
    })
})
// Get an especific WorkStation
router.get('/:_id', auth.ensureAuthenticated, async (req, res) => {
    const workStations = await WorkStation.findById(req.params._id)
    res.json({
        status: 200,
        workStations
    })
})
// Save a new WorkStation
router.post('/', auth.ensureAuthenticated, async (req, res) => {
    const workStation = new WorkStation(req.body)
    const existingWorkStation = await WorkStation.find({ boxNumber: workStation.boxNumber, adress: workStation.adress })
    if (existingWorkStation.length === 0) {
        await workStation.save()
        res.json({
            status: 200
        })
    } else {
        res.json({
            status: 409
        })
    }
})
// Edit an existing WorkStation
router.put('/:_id', auth.ensureAuthenticated, async (req, res) => {
    await WorkStation.findByIdAndUpdate(req.params._id, req.body)
    res.json({
        status: 200
    })
})
// Delete a WorkStation
router.delete('/:_id', auth.ensureAuthenticated, async (req, res) => {
    await WorkStation.findByIdAndDelete(req.params._id)
    res.json({
        status: 200
    });
})

// Search WorkStations by adress
router.get('/search/:adress', auth.ensureAuthenticated, async (req, res) => {
    const workStations = await WorkStation.find({ adress: req.params.adress })
    if (workStations.length != 0) {
        res.json({
            status: 200,
            workStations
        })
    } else {
        res.json({
            status: 204
        })
    }
})

router.get('/address/all', auth.ensureAuthenticated, async (req, res) => {
    const stationAdresses = await WorkStation.find().distinct('adress')
    res.json({
        status: 200,
        stationAdresses
    });
})

module.exports = router