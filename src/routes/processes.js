const express = require('express')
var auth = require('../middleware')
const router = express.Router()

const Process = require('../models/Process')
// Get all Processes
router.get('/', auth.ensureAuthenticated, async (req, res) => {
    const process = await Process.find()
    res.json({
        status: 200,
        process
    })
})
// Get an especific Process
router.get('/:_id', auth.ensureAuthenticated, async (req, res) => {
    const process = await Process.findById(req.params._id)
    res.json({
        status: 200,
        process
    })
})
// Save a new Process
router.post('/', auth.ensureAuthenticated, async (req, res) => {
    const process = new Process(req.body)
    await process.save()
    res.json({
        status: 200
    })
})
// Edit an existing Process
router.put('/:_id', auth.ensureAuthenticated, async (req, res) => {
    await Process.findByIdAndUpdate(req.params._id, req.body)
    res.json({
        status: 200
    })
})
// Delete a Process
router.delete('/:_id', auth.ensureAuthenticated, async (req, res) => {
    await Process.findByIdAndDelete(req.params._id)
    res.json({
        status: 200
    })
})

module.exports = router