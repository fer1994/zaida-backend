const express = require('express')
var auth = require('../middleware')
const router = express.Router()

const Product = require('../models/Product')

//Get all Products
router.get('/', auth.ensureAuthenticated, async (req, res) => {
    const products = await Product.find()
    if (products) {
        res.json({
            status: 200,
            products
        })
    } else {
        res.json({
            status: 500
        })
    }
})

// Get an especific Product
router.get('/:_id', auth.ensureAuthenticated, async (req, res) => {
    const product = await Product.findById(req.params._id)
    if (product) {
        res.json({
            status: 200,
            product
        })
    } else {
        res.json({
            status: 404
        })
    }
})

// Save a new Product
router.post('/', auth.ensureAuthenticated, async (req, res) => {
    const product = new Product(req.body)
    const existingProduct = await Product.find({ 'name': product.name, 'description': product.description })
    console.log('lenght de existingProduct: ', existingProduct.length)
    if (existingProduct.length === 0) {
        await product.save()
        res.json({
            status: 200
        })
    } else {
        res.json({
            status: 409
        })
    }
})

// Edit an existing Product
router.put('/:_id', auth.ensureAuthenticated, async (req, res) => {
    await Product.findByIdAndUpdate(req.params._id, req.body)
    res.json({
        status: 200
    })
})

// Delete a Product
router.delete('/:_id', auth.ensureAuthenticated, async (req, res) => {
    await Product.findByIdAndDelete(req.params._id)
    res.json({
        status: 200
    })
})

module.exports = router