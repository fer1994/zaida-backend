const express = require('express')
const config = require('../config');
const router = express.Router()


router.get('/', async (req, res) => {
    res.json({tag: config.BACKEND_TAG,
              status: 200})
})

module.exports = router