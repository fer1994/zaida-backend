const express = require('express')
var auth = require('../middleware')
const router = express.Router()
const Turn = require('../models/Turn')
const Employee = require('../models/Employee')
const service = require('../service.js');

// Get all Turns
router.get('/', auth.ensureAuthenticated, async (req, res) => {
    const turns = await Turn.find()
    res.json(turns)
})

// Get all Turns that have not been finalized --  (POSIBLES ESTADOS): opened - closed
router.get('/search/', auth.ensureAuthenticated, async (req, res) => {
    const turns = await Turn.find({ state: 'opened' })
    if (turns.length != 0) {
        res.json(turns)
    } else {
        res.json({
            status: 'All the turns have already been finalized, or there are still no turns.'
        })
    }
})

// Get turns by employee id
router.get('/search/:_id', auth.ensureAuthenticated, async (req, res) => {
    let state = (req.query.state || 'opened')
    const openTurns = await Turn.find({ state: state })
    let turnsToBack = [];
    for (let i = 0; i < openTurns.length; i++) {
        if (openTurns[i].employee && openTurns[i].employee._id == req.params._id) {
            turnsToBack.push(openTurns[i])
        }
    }
    if (turnsToBack.length === 0) {
        res.json({
            status: 204
        })
    }
    if (turnsToBack) {
        res.json({
            status: 200,
            turn: turnsToBack
        })
    } else {
        res.json({
            status: 204,
        })
    }
});

// Count all the turns that dont have assigned users, of a particular station address
router.get('/workstation/:group', auth.ensureAuthenticated, async (req, res) => {
    const turnsToSend = await Turn.find({
        state: null,
        group: req.params.group
    })
    if (turnsToSend) {
        res.json({
            status: 200,
            NumberOfTurns: turnsToSend.length
        })
    } else {
        res.json({
            status: 500
        })
    }
})

// Get and autoassigned the next turn from the queue
router.get('/search/attend/:_id/:group', auth.ensureAuthenticated, async (req, res) => {
    const employee = await Employee.findById(req.params._id)
    if (employee) {
        const turnToSend = await Turn.findOne(
            {
                state: null,
                group: req.params.group
            }, {},
            {
                sort: { 'priority': 'desc', 'createdAt': 'asc' }
            })
        if (turnToSend) {
            turnToSend.state = 'opened';
            turnToSend.employee = employee;
            const turnToBack = await Turn.findByIdAndUpdate(turnToSend._id, turnToSend)
            res.json({
                status: 200,
                turn: turnToBack
            })
        } else {
            res.json({
                status: 204
            })
        }
    } else {
        res.json({
            status: 204
        })
    }
})

// Get an especific Turn
router.get('/:_id', auth.ensureAuthenticated, async (req, res) => {
    const turn = await Turn.findById(req.params._id)
    res.json(turn)
})

// Create new Turn
router.post('/', auth.ensureAuthenticated, async (req, res) => {
    const turn = new Turn(req.body)
    const token = req.headers.authorization.replace("Bearer ", "")
    turn.createdAt = new Date();
    const nextNumber = await service.getNextTurnNumber(turn)
    turn.number = nextNumber
    turn.group = req.body.group
    turn.priority = req.body.priority
    turn.state = req.body.state
    turn.employee = req.body.employee
    turn.code = req.body.group.substring(0, 2).toUpperCase() + turn.number
    turn.save()
    service.send2Graylog(turn._doc, token, 'new_turn')
    res.json({
        status: 201,
        turn: turn
    })

})

// Edit an existing Turn
router.put('/:_id', auth.ensureAuthenticated, async (req, res) => {
    await Turn.findByIdAndUpdate(req.params._id, req.body)
    // TODO add token from user instaed of string "token"
    const token = req.headers.authorization.replace("Bearer ", "")

    let turn = req.body;
    const arrayProd = []
    const arrayCat = []

    await turn.products.forEach ( prod => {
        arrayProd.push(prod.name)

        prod.categories.forEach( category => {
            if(category.name && prod.name !== category.name) {
                arrayCat.push(category.name)
            }
        })
    })

    turn.products = {
        prodname: arrayProd,
        category: arrayCat
    };

    service.send2Graylog(turn, token, 'update_turn')
    res.json({
        status: 200
    })
});

// Delete a Turn
router.delete('/:_id', auth.ensureAuthenticated, async (req, res) => {
    await Turn.findByIdAndDelete(req.params._id)
    res.json({
        status: 200
    })
})

module.exports = router
