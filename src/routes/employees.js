const express = require('express')
var bcrypt = require('bcryptjs')
var service = require('../service.js');
var auth = require('../middleware')
const router = express.Router()

const Employee = require('../models/Employee')

// Get all Employees
router.get('/', auth.ensureAuthenticated, async (req, res) => {
    const employees = await Employee.find()
    res.json({
        status: 200,
        employees
    })
})

// Get an especific Employee
router.get('/:_id', auth.ensureAuthenticated, async (req, res) => {
    const employees = await Employee.findById(req.params._id)
    res.json({
        status: 200,
        employees
    })
})

// Create new Employee
router.post('/', auth.ensureAuthenticated, async (req, res) => {
    const employee = new Employee(req.body)
    const existingEmployee = await Employee.find({ 'email': employee.email, 'workStation.boxNumber': employee.workStation.boxNumber, 'workStation.adress': employee.workStation.adress })
    if (existingEmployee.length === 0) {
        const passToHash = req.body.password;
        bcrypt.genSalt(10, function (err, salt) {
            bcrypt.hash(passToHash, salt, async function (err, hash) {
                employee.password = hash
                await employee.save()
                res.json({
                    status: 200
                })
            })
        })
    } else {
        res.json({
            status: 409
        })
    }
})

// Edit an existing Employee
router.put('/:_id', auth.ensureAuthenticated, async (req, res) => {
    if (req.body.password) { // contraseña vacia, se esta actualizando desde Staff
        bcrypt.genSalt(10, function (err, salt) {
            bcrypt.hash(req.body.password, salt, async function (err, hash) {
                req.body.password = hash
                await Employee.findByIdAndUpdate(req.params._id, req.body)
                res.json({
                    status: 200
                })
            })
        })
    } else { // se intenta actualizar la contraseña
        await Employee.findByIdAndUpdate(req.params._id, req.body)
        res.json({
            status: 200
        })
    }
})

// Delete an Employee
router.delete('/:_id', auth.ensureAuthenticated, async (req, res) => {
    await Employee.findByIdAndDelete(req.params._id)
    res.json({
        status: 200
    })
})
// Authenticate a Employee (NO ESTA EN SWAGGER)
router.post('/login', async (req, res, next) => {
    const employee = await Employee.find({ email: req.body.username }, function (err, docs) {
        if (docs.length === 0) {
            res.json({
                status: 204
            })
        } else {
            for (let i = 0; i < docs.length; i++) {
                bcrypt.compare(req.body.pass, docs[i].password, async function (err, result) {
                    if (result) {
                        let token = await service.createToken(docs[i])
                        docs[i].password = undefined;
                        service.send2Graylog(docs[i]._doc, token, 'login')
                        res.json({
                            user: docs[i],
                            token: token,
                            status: 200
                        })
                    } else {
                        res.json({
                            status: 204
                        })
                    }
                })
            }
        }
    })

    /*     (Compare an unencrypted password with an encrypted one)
        bcrypt.compare("matias12", hash, function (err, res) {
            console.log('resultado: ', res)
        }); */

})

//Logout (NO ESTA EN SWAGGER)
router.post('/logout', auth.ensureAuthenticated, (req, res) => {
    const token = req.headers.authorization.replace("Bearer ", "")
    service.send2Graylog(req.body, token, 'logout')
    res.json({
        status: 200
    })
})

// Search Employees by adress
router.get('/search/:userAdress/', auth.ensureAuthenticated, async (req, res) => {
    const employees = await Employee.find({ 'workStation.adress': req.params.userAdress })
    res.json({
        status: 200,
        employees
    })
})
module.exports = router